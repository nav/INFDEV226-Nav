package nl.nav.infdev226;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static nl.nav.infdev226.Monitoring.*;

public class DataLoader {
    /**
     * Let's try out the JCF a bit.
     * Sorry, no tests this time.
     *
     * You will:
     * 1. choose a JCF-data structure and argument why you chose that one
     *      - Going to use ArrayList as this is the most used one (i think..)
     *
     * 2. fill it with some data
     *      - Done, see below how I use the Monitoring.csv to get some.
     *
     * 3. Explore and use existing JCF-algorithms
     *      - Doing sort and shuffling the array
     * -. along the way you'll also learn about generics, comparators
     *
     * Be sure to read up on all this stuff. For example here:
     * http://docs.oracle.com/javase/tutorial/collections/TOC.html
     *
     * 10 December 2015 by Nav Appaiya
     * navarajh@gmail.com || 0892601@hr.nl
     */
    public static void main(String[] args) {

        // TODO change to a lower level interface of you liking
        // BufferedReader br = getFileReader("Monitoring_SMALL.csv");
        // Collection<Monitoring> col = getCollection(br);
        BufferedReader raw = getFileReader("Monitoring_SMALL.csv");
        // TODO sort the collection on 'beginTime'
        ArrayList arrayList = getCollection(raw);
        System.out.println(arrayList);System.exit(0);

        Monitoring mm = new Monitoring();
        BegintTimeComperator bc = new BegintTimeComperator();
        Collections.sort(arrayList, bc);

        // TODO now sort the collection on 'type'
        Collections.sort(arrayList, new Comparator<Monitoring>() {
            @Override
            public int compare(Monitoring o1, Monitoring o2) {
                return o1.getType().compareTo(o2.getType());
            }
        });
        for (Object m: arrayList){
             System.out.println(m);
        }

        //TODO shuffle the collection in random order
        // System.out.println(arrayList);
        Collections.shuffle(arrayList);
        // System.out.println(arrayList);

        //TODO find the lowest 'UnitId'
        Collections.min(arrayList);
        // System.out.println(arrayList);

        //TODO find the maximum 'beginTime'
        Iterator iter = arrayList.iterator();
        System.out.println(arrayList.get(1));
        System.exit(0);
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }
    }

    /**
     * Takes a csv-file (BufferedReader) and returns a
     * collection (which you have to choose).
     * @param br
     * @return
     */
    private static ArrayList<Monitoring> getCollection(BufferedReader br){
        Monitoring mon = new Monitoring();
        String currentLine;
        ArrayList<Monitoring> collection = new ArrayList<Monitoring>();
        try {
            br.readLine(); //skip the first line
            String line = br.readLine();
            while(line != null) {
                mon = mon.create(line);
                collection.add(mon);
                line = br.readLine();
                br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
//        System.out.println("\nFINAL RETURN COLLECTION:\n "+collection+"\n\n");
        return collection;
    }

    private static BufferedReader getFileReader(String file) {
        try {
            return Files.newBufferedReader(Paths.get(file), StandardCharsets.ISO_8859_1);
        } catch (IOException ioe) {
            System.err.println("err: " + ioe);
        }
        return null;
    }
}
